// DLLShortcutSmart.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <string>
#include <vector>
#include <windowsx.h>
#include <shellapi.h>

using namespace std;

#define CTRL	1
#define SHIFT	3
#define ALT		5
#define	ASCIIKEY	10
#define EXPORT _declspec(dllexport)

HHOOK hHookKey = NULL;
HINSTANCE hInst;

struct SHORTCUT_DATA {
	int key;
	wstring name;
	wstring url;
};

bool isCtrl, isShift, isAlt, isKey;
int valueKey;
vector<SHORTCUT_DATA> listShortcutData;

EXPORT void _updateListShortcutData(vector<SHORTCUT_DATA> data)
{
	listShortcutData.resize(0);
	for (int i = 0; i < data.size(); i++)
		listShortcutData.push_back(data[i]);
}
LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);

LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)
		return CallNextHookEx(NULL, nCode, wParam, lParam);
	KBDLLHOOKSTRUCT *info = (KBDLLHOOKSTRUCT *)lParam;
	if (wParam == WM_KEYDOWN) {
		if (GetAsyncKeyState(VK_CONTROL))
			isCtrl = true;
		if (GetAsyncKeyState(VK_SHIFT))
			isShift = true;
		if (GetAsyncKeyState(VK_MENU))
			isAlt = true;
		// 0-9, A-Z
		if (((info->vkCode>= 0x30 && info->vkCode<=0x39)||(info->vkCode >= 0x41 && info->vkCode <=0x5A)) && (isCtrl || isShift || isAlt)) {
			isKey = true;
			valueKey = (int)info->vkCode;
		}
		if((isCtrl||isShift||isAlt) && !isKey)
			return CallNextHookEx(NULL, nCode, wParam, lParam);

		int value = isCtrl*CTRL + isShift*SHIFT + isAlt*ALT + valueKey*ASCIIKEY;
		//wstring code = L"explorer " ;

		for (int i = 0; i < listShortcutData.size(); i++)
		{
			if (value == listShortcutData[i].key)
			{
				//code += listShortcutData[i].url;
				//system(string(code.begin(), code.end()).c_str());
				STARTUPINFO si;
				PROCESS_INFORMATION pi;

				// set the size of the structures
				ZeroMemory(&si, sizeof(si));
				si.cb = sizeof(si);
				ZeroMemory(&pi, sizeof(pi));

				// start the program up
				CreateProcess(listShortcutData[i].url.c_str(),   // the path
					NULL,        // Command line
					NULL,           // Process handle not inheritable
					NULL,           // Thread handle not inheritable
					FALSE,          // Set handle inheritance to FALSE
					0,              // No creation flags
					NULL,           // Use parent's environment block
					NULL,           // Use parent's starting directory 
					&si,            // Pointer to STARTUPINFO structure
					&pi          // Pointer to PROCESS_INFORMATION structure
				);
				// Close process and thread handles. 
				CloseHandle(pi.hProcess);
				CloseHandle(pi.hThread);

				break;
			}
		}
		return CallNextHookEx(NULL, nCode, wParam, lParam);
	}
	if (wParam == WM_KEYUP) {
		if (info->vkCode == VK_LCONTROL || info->vkCode == VK_RCONTROL)
			isCtrl = false;
		if (info->vkCode == VK_LSHIFT || info->vkCode == VK_RSHIFT)
			isShift = false;
		if (info->vkCode == VK_LMENU || info->vkCode == VK_RMENU)
			isAlt = false;
		if ((info->vkCode >= 0x30 && info->vkCode <= 0x39) || (info->vkCode >= 0x41 && info->vkCode <= 0x5A))
			isKey = false;
	}
	return CallNextHookEx(NULL, nCode, wParam, lParam);
}

EXPORT void _doInstallHook(HWND hWnd)
{
	if (hHookKey != NULL) return;
	hHookKey = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)KeyboardProc, hInst, 0);
	//if (hHookKey)
	//	MessageBox(hWnd, L"Setup hook successfully", L"Result", MB_OK);
	//else
	//	MessageBox(hWnd, L"Setup hook fail", L"Result", MB_OK);
}

EXPORT void _doRemoveHook(HWND hWnd)
{
	if (hHookKey == NULL) return;
	UnhookWindowsHookEx(hHookKey);
	hHookKey = NULL;
	//MessageBox(hWnd, L"Remove hook successfully", L"Result", MB_OK);
}