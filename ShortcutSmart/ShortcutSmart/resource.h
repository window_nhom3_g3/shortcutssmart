//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ShortcutSmart.rc
//
#define IDC_MYICON                      2
#define IDD_SHORTCUTSMART_DIALOG        102
#define IDD_ADDSHORTCUT                 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDD_UPDATESHORTUT               104
#define IDM_EXIT                        105
#define IDD_DELETESHORTCUT              105
#define IDI_SHORTCUTSMART               107
#define IDI_SMALL                       108
#define IDC_SHORTCUTSMART               109
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDD_VIEWSHORTCUT                131
#define IDE_PATH                        1000
#define IDB_BROWSE                      1001
#define IDE_NAME                        1002
#define IDE_KEY                         1003
#define IDCB_CTRL                       1004
#define IDCB_SHIFT                      1005
#define IDCB_ALT                        1006
#define IDC_LIST                        1007
#define IDM_SHORTCUT_ADD                32771
#define IDM_SHORTCUT_UPDATE             32772
#define IDM_SHORTCUT_DELETE             32773
#define IDM_SHORTCUT_VIEW               32774
#define ID_ACCELERATOR32775             32775
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
