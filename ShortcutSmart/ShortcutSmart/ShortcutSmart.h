#pragma once

#include "resource.h"
#include <string>
#include <vector>
#include <commdlg.h>
#include <vector>

#define CTRL	1
#define SHIFT	3
#define ALT		5
#define	ASCIIKEY	10

struct SHORTCUT_DATA
{
	int key;
	std::wstring name;
	std::wstring url;
};

void OnShortcutAdd(HWND hWnd, HINSTANCE hInst);
void OnShortcutUpdate(HWND hWnd, HINSTANCE hInst);
void OnShortcutDelete(HWND hWnd, HINSTANCE hInst);
void OnShortcutView(HWND hWnd, HINSTANCE hInst);
INT_PTR CALLBACK AddShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK UpdateShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK DeleteShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ViewShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

int GetDataFromKey(int key, bool &bCtrl, bool &bShift, bool &bAlt);

