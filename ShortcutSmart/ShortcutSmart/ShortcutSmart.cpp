﻿// ShortcutSmart.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "ShortcutSmart.h"
#include <shellapi.h>
#include <commdlg.h>
#include <Windows.h>
#include <fstream>
#include "Shlwapi.h"


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
static std::vector<SHORTCUT_DATA> data;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void doInstallHook(HWND);
void doRemoveHook(HWND);
void doUpdateListShortcutData(std::vector<SHORTCUT_DATA> newData);
BOOL DirectoryExists(LPCTSTR szPath);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_SHORTCUTSMART, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SHORTCUTSMART));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SHORTCUTSMART));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_SHORTCUTSMART);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

BOOL DirectoryExists(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
	case WM_CREATE:
	{
		doInstallHook(hWnd);
		doUpdateListShortcutData(data);
		break;
	}
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
			case IDM_SHORTCUT_ADD:
				OnShortcutAdd(hWnd, hInst);
				break;
			case IDM_SHORTCUT_UPDATE:
				OnShortcutUpdate(hWnd, hInst);
				break;
			case IDM_SHORTCUT_DELETE:
				OnShortcutDelete(hWnd, hInst);
				break;
			case IDM_SHORTCUT_VIEW:
				OnShortcutView(hWnd, hInst);
				break;

            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		doRemoveHook(hWnd);
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void OnShortcutAdd(HWND hWnd, HINSTANCE hInst)
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_ADDSHORTCUT), hWnd, AddShortcutDlgProc);
}

void OnShortcutUpdate(HWND hWnd, HINSTANCE hInst)
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_UPDATESHORTUT), hWnd, UpdateShortcutDlgProc);
}

void OnShortcutDelete(HWND hWnd, HINSTANCE hInst)
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_DELETESHORTCUT), hWnd,DeleteShortcutDlgProc);
}

void OnShortcutView(HWND hWnd, HINSTANCE hInst)
{
	DialogBox(hInst, MAKEINTRESOURCE(IDD_VIEWSHORTCUT), hWnd, ViewShortcutDlgProc);
}

void OpenDialogPath(HWND hWnd, TCHAR szFile[])
{
	OPENFILENAME ofn;
	TCHAR szFilter[] = TEXT("Application\0*.exe\0");
	szFile[0] = '0';
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 2;
	ofn.lpstrInitialDir = NULL;
	ofn.nMaxFile = 256;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn))
		szFile = ofn.lpstrFile;
}

INT_PTR CALLBACK AddShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR szFile[256];
	static SHORTCUT_DATA ad, *pad;

	switch (message)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, IDE_PATH));
		SendMessage(GetDlgItem(hDlg, IDE_KEY), EM_LIMITTEXT, 1, 0);
		/*pad = (SHORTCUT_DATA*)lParam;
		ad = *pad;*/
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDB_BROWSE:
			OpenDialogPath(hDlg, szFile);
			SetWindowText(GetDlgItem(hDlg, IDE_PATH), szFile);
			break;
		case IDE_KEY:
			break;
		case IDOK:
		{
			bool bCtrl = SendMessage(GetDlgItem(hDlg, IDCB_CTRL), BM_GETCHECK, 0, 0) == BST_CHECKED;
			bool bShift = SendMessage(GetDlgItem(hDlg, IDCB_SHIFT), BM_GETCHECK, 0, 0) == BST_CHECKED;
			bool bAlt = SendMessage(GetDlgItem(hDlg, IDCB_ALT), BM_GETCHECK, 0, 0) == BST_CHECKED;
			TCHAR buff[256];
			GetWindowText(GetDlgItem(hDlg, IDE_KEY), buff, 256);
			if (((buff[0] >= L'A' && buff[0] <= L'Z') || (buff[0] >= L'a' && buff[0] <= L'z') || (buff[0] >= L'0' && buff[0] <= L'9'))
				&& (GetWindowTextLength(GetDlgItem(hDlg, IDE_PATH)) != 0)
				&& (GetWindowTextLength(GetDlgItem(hDlg, IDE_NAME)) != 0)
				&& (bCtrl || bShift || bAlt))
			{
				int key = bCtrl*CTRL + bAlt*ALT + bShift*SHIFT + toupper(buff[0]) * ASCIIKEY; 
				SHORTCUT_DATA temp;
				data.push_back(temp);
				int idx = data.size() - 1;
				data[idx].key = key;
				WCHAR buff[256];

				GetWindowText(GetDlgItem(hDlg, IDE_NAME), buff, 256);
				data[idx].name = buff;

				GetWindowText(GetDlgItem(hDlg, IDE_PATH), buff, 256);
				data[idx].url = buff;

				std::fstream f;
				f.open(std::string(data[idx].url.begin(), data[idx].url.end()), std::ios::in);
				if (!f.good())
				{
					MessageBox(hDlg, L"Lỗi đường dẫn!\nVui lòng nhập lại!", L"ERROR", MB_ICONERROR | MB_OK);
					data.erase(data.begin() + idx);
					SetWindowText(GetDlgItem(hDlg, IDE_PATH), L"");
					SetFocus(GetDlgItem(hDlg, IDE_PATH));
					break;
				}
				f.close();
			}
			else
			{
				MessageBox(hDlg, L"Lỗi nhập!\nVui lòng nhập lại!", L"ERROR", MB_ICONERROR | MB_OK);
				SetWindowText(GetDlgItem(hDlg, IDE_KEY), L"");
				SetFocus(GetDlgItem(hDlg, IDE_KEY));
				break;
			}

			doUpdateListShortcutData(data);
			EndDialog(hDlg, TRUE);
			return TRUE;
		}
		case IDCANCEL:
			EndDialog(hDlg, IDCANCEL);
			return (INT_PTR)TRUE;
		}
	case WM_PAINT:
		break;
	case WM_CLOSE:
		EndDialog(hDlg, IDCANCEL);
		break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK UpdateShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR szFile[256];
	switch (message)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, IDC_LIST));
		SendMessage(GetDlgItem(hDlg, IDE_KEY), EM_LIMITTEXT, 1, 0);
		for (int i = 0; i < data.size(); i++)
			SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_ADDSTRING, 0, (LPARAM)&data[i].name[0]);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDB_BROWSE:
			OpenDialogPath(hDlg, szFile);
			SetWindowText(GetDlgItem(hDlg, IDE_PATH), szFile);
			break;
		case IDE_KEY:
			break;

		case IDC_LIST:
		{
			int idx;
			if (HIWORD(wParam) == CBN_SELCHANGE)
			{
				idx = SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_GETCURSEL, 0, 0);
				for (int i = IDE_PATH; i <= IDCB_ALT; i++)
					EnableWindow(GetDlgItem(hDlg, i), TRUE);
				SetWindowText(GetDlgItem(hDlg, IDE_PATH), &data[idx].url[0]);
				SetWindowText(GetDlgItem(hDlg, IDE_NAME), &data[idx].name[0]);
				bool bCtr, bShift, bAlt;
				wchar_t key = GetDataFromKey(data[idx].key, bCtr, bShift, bAlt);
				std::wstring wskey = L"a";
				wskey[0] = key;
				SendMessage(GetDlgItem(hDlg, IDCB_CTRL), BM_SETCHECK, bCtr, 0);
				SendMessage(GetDlgItem(hDlg, IDCB_SHIFT), BM_SETCHECK, bShift, 0);
				SendMessage(GetDlgItem(hDlg, IDCB_ALT), BM_SETCHECK, bAlt, 0);
				SetWindowText(GetDlgItem(hDlg, IDE_KEY), wskey.c_str());
			}
		}
			break;
		case IDOK:
		{
			int idx = SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_GETCURSEL, 0, 0);
			bool bCtrl = SendMessage(GetDlgItem(hDlg, IDCB_CTRL), BM_GETCHECK, 0, 0) == BST_CHECKED;
			bool bShift = SendMessage(GetDlgItem(hDlg, IDCB_SHIFT), BM_GETCHECK, 0, 0) == BST_CHECKED;
			bool bAlt = SendMessage(GetDlgItem(hDlg, IDCB_ALT), BM_GETCHECK, 0, 0) == BST_CHECKED;
			TCHAR buff[256];
			GetWindowText(GetDlgItem(hDlg, IDE_KEY), buff, 256);
			if (((buff[0] >= L'A' && buff[0] <= L'Z') || (buff[0] >= L'a' && buff[0] <= L'z') || (buff[0] >= L'0' && buff[0] <= L'9'))
				&& (GetWindowTextLength(GetDlgItem(hDlg, IDE_PATH)) != 0)
				&& (GetWindowTextLength(GetDlgItem(hDlg, IDE_NAME)) != 0)
				&& (bCtrl || bShift || bAlt)
				&& idx >= 0)
			{
				int key = bCtrl*CTRL + bAlt*ALT + bShift*SHIFT + toupper(buff[0]) * ASCIIKEY;
				data[idx].key = key;
				WCHAR buff[256];
				GetWindowText(GetDlgItem(hDlg, IDE_NAME), buff, 256);
				data[idx].name = buff;
				GetWindowText(GetDlgItem(hDlg, IDE_PATH), buff, 256);
				data[idx].url = buff;
				
				std::fstream f;
				f.open(std::string(data[idx].url.begin(), data[idx].url.end()), std::ios::in);
				if (!f.good())
				{
					MessageBox(hDlg, L"Lỗi đường dẫn!\nVui lòng nhập lại!", L"ERROR", MB_ICONERROR | MB_OK);
					data.erase(data.begin() + idx);
					SetWindowText(GetDlgItem(hDlg, IDE_PATH), L"");
					SetFocus(GetDlgItem(hDlg, IDE_PATH));
					break;
				}
				f.close();
			}
			else
			{
				MessageBox(hDlg, L"Lỗi nhập!\nVui lòng nhập lại!", L"ERROR", MB_ICONERROR | MB_OK);
				SetWindowText(GetDlgItem(hDlg, IDE_KEY), L"");
				SetFocus(GetDlgItem(hDlg, IDE_KEY));
				break;
			}
			doUpdateListShortcutData(data);
			EndDialog(hDlg, TRUE);
			return TRUE;
		}
			break;
		case IDCANCEL:
			EndDialog(hDlg, IDCANCEL);
			return (INT_PTR)TRUE;
		}
		break;
	case WM_PAINT:
		break;
	case WM_CLOSE:
		EndDialog(hDlg, IDCANCEL);
		break;
	}
	return (INT_PTR)FALSE;
}


INT_PTR CALLBACK DeleteShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, IDC_LIST));
		SendMessage(GetDlgItem(hDlg, IDE_KEY), EM_LIMITTEXT, 1, 0);
		for (int i = 0; i < data.size(); i++)
			SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_ADDSTRING, 0, (LPARAM)&data[i].name[0]);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_LIST:
		{
			int idx;

			if (HIWORD(wParam) == CBN_SELCHANGE)
			{
				idx = SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_GETCURSEL, 0, 0);
				SetWindowText(GetDlgItem(hDlg, IDE_PATH), &data[idx].url[0]);
				SetWindowText(GetDlgItem(hDlg, IDE_NAME), &data[idx].name[0]);
				bool bCtr, bShift, bAlt;
				wchar_t key = GetDataFromKey(data[idx].key, bCtr, bShift, bAlt);
				std::wstring wskey = L"a";
				wskey[0] = key;
				SendMessage(GetDlgItem(hDlg, IDCB_CTRL), BM_SETCHECK, bCtr, 0);
				SendMessage(GetDlgItem(hDlg, IDCB_SHIFT), BM_SETCHECK, bShift, 0);
				SendMessage(GetDlgItem(hDlg, IDCB_ALT), BM_SETCHECK, bAlt, 0);
				SetWindowText(GetDlgItem(hDlg, IDE_KEY), wskey.c_str());
			}
			break;
		}
		case IDOK:
		{
			int idx = SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_GETCURSEL, 0, 0);
			data.erase(data.begin() + idx);
			doUpdateListShortcutData(data);
			EndDialog(hDlg, TRUE);
			return TRUE;
		}
		case IDCANCEL:
			EndDialog(hDlg, IDCANCEL);
			return (INT_PTR)TRUE;
		}
		break;
		case WM_PAINT:
			break;
		case WM_CLOSE:
			EndDialog(hDlg, IDCANCEL);
			break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK ViewShortcutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, IDC_LIST));
		SendMessage(GetDlgItem(hDlg, IDE_KEY), EM_LIMITTEXT, 1, 0);
		for (int i = 0; i < data.size(); i++)
			SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_ADDSTRING, 0, (LPARAM)&data[i].name[0]);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_LIST:
		{
			int idx;

			if (HIWORD(wParam) == CBN_SELCHANGE)
			{
				idx = SendMessage(GetDlgItem(hDlg, IDC_LIST), CB_GETCURSEL, 0, 0);
				SetWindowText(GetDlgItem(hDlg, IDE_PATH), &data[idx].url[0]);
				SetWindowText(GetDlgItem(hDlg, IDE_NAME), &data[idx].name[0]);
				bool bCtr, bShift, bAlt;
				wchar_t key = GetDataFromKey(data[idx].key, bCtr, bShift, bAlt);
				std::wstring wskey = L"a";
				wskey[0] = key;
				SendMessage(GetDlgItem(hDlg, IDCB_CTRL), BM_SETCHECK, bCtr, 0);
				SendMessage(GetDlgItem(hDlg, IDCB_SHIFT), BM_SETCHECK, bShift, 0);
				SendMessage(GetDlgItem(hDlg, IDCB_ALT), BM_SETCHECK, bAlt, 0);
				SetWindowText(GetDlgItem(hDlg, IDE_KEY), wskey.c_str());
			}
			break;
		}
		case IDCANCEL:
			EndDialog(hDlg, IDCANCEL);
			return (INT_PTR)TRUE;
		}
		break;
	case WM_PAINT:
		break;
	case WM_CLOSE:
		EndDialog(hDlg, IDCANCEL);
		break;
	}
	return (INT_PTR)FALSE;
}

void doInstallHook(HWND hWnd)
{
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"DLLShortcutSmart.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doInstallHook");
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}

void doRemoveHook(HWND hWnd)
{
	typedef VOID(*MYPROC)(HWND);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;
	hinstLib = LoadLibrary(L"DLLShortcutSmart.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_doRemoveHook");
		if (ProcAddr != NULL)
			ProcAddr(hWnd);
	}
}
void doUpdateListShortcutData(std::vector<SHORTCUT_DATA> newData)
{
	typedef VOID(*MYPROC)(std::vector<SHORTCUT_DATA>);

	HINSTANCE hinstLib;
	MYPROC ProcAddr;

	hinstLib = LoadLibrary(L"DLLShortcutSmart.dll");
	if (hinstLib != NULL) {
		ProcAddr = (MYPROC)GetProcAddress(hinstLib, "_updateListShortcutData");
		if (ProcAddr != NULL)
			ProcAddr(newData);
	}
}